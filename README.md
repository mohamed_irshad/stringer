# README #

### About ###

* Project to parse a string into to an JSON, which contains Emoticons, Links & Mentioned names
* Version 1.0

### Key notables ###

* Code inside is fully designed using `Swift1.2`
* `Autolayout` and `Sizeclass` are used where ever nessasary
* Fully `Commented` for better understanding 
* Perfomance tested

### Environment to run ###

* Open using Xcode 6.4 (or any Xcode which supports Swift 1.2) and Run it.

### Guidelines ###

* Unit test case written using `XCTest`
* Code review done manually by `Mohamed Irshad`

### Contact ###

* For quires, email to - [Irshad](irshad365@gmail.com)