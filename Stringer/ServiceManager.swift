//
//  ServiceManager.swift
//  Stringer
//
//  Created by Irshad on 8/12/15.
//  Copyright (c) 2015 imaginea. All rights reserved.
//

import Foundation

class ServiceManager: NSObject {
    
    /**
    Function to extract title out of the given url
    
    - parameter url: Destination url as String
    
    - returns: Title of the page if found else returns nil
    */
    class func getTitleFromURL(url:String) -> String? {
        if let htmlData = NSData(contentsOfURL: NSURL(string: url)!) {
            if let htmlString = NSString(data: htmlData, encoding: NSUTF8StringEncoding) {
                if let titleRange = (htmlString as String).rangeOfString("<title[^>]*>(.*?)</title>", options: .RegularExpressionSearch) {
                    let titleWithTag = (htmlString as String).substringWithRange(titleRange)
                    
                    // Some title tag might contains attribute, so just substring the title string will not be enough
                    if let _ = titleWithTag.rangeOfString(">(.*?)</title>", options: .RegularExpressionSearch) {
                        let title = titleWithTag.substringWithRange(Range<String.Index>(start:titleWithTag.startIndex.advancedBy(+7) , end: titleWithTag.endIndex.advancedBy(-8)))
                        return title
                        
                    }
                }
            }
        }
        return nil
    }
}
