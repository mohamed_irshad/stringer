//
//  ViewController.swift
//  Stringer
//
//  Created by Irshad on 8/10/15.
//  Copyright (c) 2015 irshad. All rights reserved.
//

import UIKit

class ViewController: UIViewController,TypingViewDelegate {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var dock: UIView!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var outputView: UITextView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var typingView: TypingView!
    
    @IBOutlet weak var keyboardHeight: NSLayoutConstraint!
    @IBOutlet weak var loadButtonHeight: NSLayoutConstraint!
    
    var originalLoadButtonHeight = CGFloat(0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add Border to match the theme color and beautify UI
        dock.layer.borderColor = themeColor.CGColor
        dock.layer.borderWidth = 1.0
        
        // Add notification observer for keyboard
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
    Trigred on click of 'Load default value' button
    
    - parameter sender: load button object
    */
    @IBAction func LoadButtonClicked(sender: AnyObject) {
        //"hello ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i))"
        textField1.text = defaultValue
    }
    
    /**
    Trigred on click of 'parse' button
    
    - parameter sender: Parse button object
    */
    @IBAction func parseButtonClicked(sender: AnyObject) {
        hideLoadingScreen(false)
        self.viewEndEditing()
        // Perform parsing in Backgroung thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)){
            StringParser().parseInputString(self.textField1.text! + " ") { outputString in
               
                // return back to main thread after completeing parsing
                dispatch_async(dispatch_get_main_queue()){
                    self.hideLoadingScreen(true)
                    if outputString != nil  {
                        self.outputView.text = outputString
                    } else {
                        self.outputView.text = "No valid objects found"
                    }
                }
            }
        }
    }
    
    /**
    Animate show/hide loading screen
    
    - parameter bool: Boolean value to tell should hide or not
    */
    func hideLoadingScreen(bool: Bool) {
        UIView.transitionWithView(loadingView, duration: 0.3, options: .TransitionCrossDissolve, animations:{
            self.loadingView.hidden = bool
            }, completion: nil)
    }
    
    /**
    Animate show/hide typing screen
    
    - parameter bool: Boolean value to tell should hide or not
    */
    func hideTypingScreen(bool: Bool) {
        UIView.transitionWithView(typingView, duration: 0.3, options: .TransitionCrossDissolve, animations:{
            self.typingView.hidden = bool
            }, completion: nil)
    }
    
    /**
    Typing View Delegate to hide keyboard
    */
    func viewEndEditing(){
        // As soon as the touch happens, dismis the keyboard
        textField1.resignFirstResponder()
        self.view.endEditing(true)
    }
}

// MARK: - Keyboard Notification Handlers
extension ViewController {
    /**
    UIKeyboardWillShowNotification notification responder
    
    - parameter notification: NSNotificationCenter which trigred
    */
    func keyboardWillShow(notification: NSNotification) {
        hideTypingScreen(false)
        if let info = notification.userInfo {
            if let kbFrame = info[UIKeyboardFrameEndUserInfoKey] as? NSValue{
                let keyboardFrame = kbFrame.CGRectValue()
                if let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                    keyboardHeight.constant = keyboardFrame.size.height
                    originalLoadButtonHeight = loadButtonHeight.constant
                    loadButtonHeight.constant = 0
                    loadButton.alpha = 0
                    UIView.animateWithDuration(Double(animationDuration)) {  () -> Void in
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    /**
    UIKeyboardWillHideNotification notification responder
    
    - parameter notification: NSNotificationCenter which trigred
    */
    func keyboardWillHide(notification: NSNotification) {
        hideTypingScreen(true)
        if let info = notification.userInfo {
            if let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                keyboardHeight.constant = 0
                if originalLoadButtonHeight == 0 {
                    originalLoadButtonHeight = CGFloat(30.0)
                }
                loadButtonHeight.constant = originalLoadButtonHeight
                loadButton.alpha = 1
                UIView.animateWithDuration(Double(animationDuration)) {  () -> Void in
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}
