//
//  StringParser.swift
//  Stringer
//
//  Created by Irshad on 8/12/15.
//  Copyright (c) 2015 imaginea. All rights reserved.
//

import UIKit

class StringParser: NSObject {
    
    var emotionsArray : [String] = Array()
    var mentionsArray : [String] = Array()
    var urlsArray : [[String:String]] = Array()
    var callBack:((String?)->Void)?
    
    /**
    This is the entry point of the StringParser, which intiates the parsing
    
    - parameter findStr:    String that has to be parsed
    - parameter completion: Call back handler
    */
    func parseInputString(findStr:String, completion: ((String?)->Void)?) {
        let emotionRangex = getRangeForString("\\(", onParentSring: findStr)
        let mentionRangex = getRangeForString("@", onParentSring: findStr)
        let urlRangex = getRangeForString("://", onParentSring: findStr)
        
        if completion != nil {
            callBack = completion
        }
        
        // Which ever comes first, gather it first. Do not parse the whole string multiple times for getting different sets.
        if emotionRangex.startIndex < mentionRangex.startIndex {
            if emotionRangex.startIndex < urlRangex.startIndex {
                gatherEmotions(findStr)
            } else {
                gatherUrls(findStr)
            }
        } else {
            if mentionRangex.startIndex < urlRangex.startIndex {
                gatherMentions(findStr)
            } else {
                gatherUrls(findStr)
            }
        }
        
        
        // when all conditions fail, they all become equal.
        if mentionRangex.startIndex == urlRangex.startIndex && emotionRangex.startIndex == urlRangex.startIndex {
            if mentionRangex.endIndex == urlRangex.endIndex && emotionRangex.endIndex == urlRangex.endIndex {
                if let callback = callBack {
                    callback(constructAndGetOutputDictionary())
                }
            }
        }
    }
    
    /**
    Get intial range of link, emoticons, mentions
    
    - parameter string:        regex format
    - parameter onParentSring: String which has to be parsed
    
    - returns: Returns the range if any, else returns the parent string's range
    */
    private func getRangeForString(string:String, onParentSring:String) -> Range<String.Index> {
        var range = onParentSring.rangeOfString(string, options:.RegularExpressionSearch)
        if range == nil {
            range = Range<String.Index>(start:onParentSring.endIndex,end:onParentSring.endIndex)
        }
        return range!
    }
    
    /**
    Finds Mentions in the given string
    
    - parameter findStr: - String which has to be parsed
    */
    private func gatherMentions(findStr:String) {
        let strRange = findStr.rangeOfString("@(.*?) ", options:.RegularExpressionSearch) // also check for "@(.*?)"
        if let range = strRange {
            // Trim the space at the last space & @ in the front
            let name = findStr.substringWithRange(Range<String.Index>(start:range.startIndex.advancedBy(+1), end: range.endIndex.advancedBy(-1)))
            
            // add the mentioned name to the array
            mentionsArray.append(name)
            
            // delete the parsed string values, and pass the remaing string as the input for the next iteration
            let nextString = findStr.substringWithRange(Range<String.Index>(start:range.endIndex , end: findStr.endIndex))
            parseInputString(nextString, completion: nil)
        }
    }
    
    /**
    Finds URl in the given string
    
    - parameter findStr: - String which has to be parsed
    */
    private func gatherUrls(findStr:String) {
        let strRange = findStr.rangeOfString("://(.*?) ", options:.RegularExpressionSearch) // also check for "://(.*?)"
        if let range = strRange {
            // The range found will only start from ://, but we have to fing the scheme
            // To find the scheme, substing from start of the string to the range end
            let trimedString = findStr.substringWithRange(Range<String.Index>(start:findStr.startIndex , end: range.endIndex.advancedBy(-1)))
            
            // only the last part of the array is our url
            if let url = trimedString.componentsSeparatedByString(" ").last {
                // add url to the array
                
                if let title = ServiceManager.getTitleFromURL(url) {
                    urlsArray.append([keyUrl:url,keyTitle:title])
                } else {
                    urlsArray.append([keyUrl:url])
                }
            }
            // delete the parsed string values, and pass the remaing string as the input for the next iteration
            let nextString = findStr.substringWithRange(Range<String.Index>(start:range.endIndex , end: findStr.endIndex))
            parseInputString(nextString, completion: nil)
        }
    }
    
    /**
    Finds Emoticons in the given string
    
    - parameter findStr: - String which has to be parsed
    */
    private func gatherEmotions(findStr:String) {
        let strRange = findStr.rangeOfString("\\(([^\\(\\)]*)\\)", options:.RegularExpressionSearch) // //"\\((.*?)\\)"
        // If there is a emotion found
        if let range = strRange {
            
            // calculate the length of emotion
            let emotionLength = range.startIndex.distanceTo(range.endIndex)
            
            if emotionLength < 18 && !(emotionLength < 3) {
                var found = findStr.substringWithRange(range)
                
                // Delete the braces inside the string
                found = found.stringByReplacingOccurrencesOfString(")", withString: "", options: .LiteralSearch).stringByReplacingOccurrencesOfString("(", withString: "", options: .LiteralSearch)
                emotionsArray.append(found)
            }
            // delete the parsed string values, and pass the remaing string as the input for the next iteration
            let nextString = findStr.substringWithRange(Range<String.Index>(start:range.endIndex , end: findStr.endIndex))
            parseInputString(nextString, completion: nil)
        }
    }
    
    /**
    Final place where all the arrays gather into a single JSON string
    
    - returns: Aligned JSON string if there are any parsable objects available, else nil
    */
    private func constructAndGetOutputDictionary() -> String? {
        var finalDict = [String : Array<AnyObject>]()
        if mentionsArray.count != 0 {
            finalDict[keyMentions] = mentionsArray
        }
        if emotionsArray.count != 0 {
            finalDict[keyEmotions] = emotionsArray
        }
        if urlsArray.count != 0 {
            finalDict[keyLinks] = urlsArray
        }
        
        // Serialize the dictionary into printable format
        if finalDict.count != 0 {
            var err:NSError?
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(finalDict, options: .PrettyPrinted )
                if let jsonString = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return jsonString as String
                }
            } catch let error as NSError {
                err = error
            }
        }
        return nil
    }
    
}
