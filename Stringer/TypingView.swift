//
//  TypingView.swift
//  Stringer
//
//  Created by Irshad on 8/12/15.
//  Copyright (c) 2015 imaginea. All rights reserved.
//

import UIKit

protocol TypingViewDelegate {
    func viewEndEditing()
}

class TypingView: UIView {
    
    @IBOutlet var delegate: AnyObject?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            if touch.phase == .Began {
                if let delegateClass = delegate as? TypingViewDelegate {
                    delegateClass.viewEndEditing()
                }
            }
        }
    }
}
