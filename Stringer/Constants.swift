//
//  Constants.swift
//  Stringer
//
//  Created by Irshad on 8/12/15.
//  Copyright (c) 2015 imaginea. All rights reserved.
//

import UIKit

let themeColor = UIColor(red: 23.0/255.0, green: 60.0/255.0, blue: 111.0/255.0, alpha: 1.0)

let keyEmotions = "emotions"
let keyMentions = "mentions"
let keyLinks = "links"

let keyUrl = "url"
let keyTitle = "title"

let defaultValue = "@bob @john (success) such a cool feature https://www.atlassian.com/software/jira"
