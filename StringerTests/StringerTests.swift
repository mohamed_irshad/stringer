//
//  StringerTests.swift
//  StringerTests
//
//  Created by Irshad on 8/10/15.
//  Copyright (c) 2015 irshad. All rights reserved.
//

import UIKit
import XCTest
import Stringer

class StringerTests: XCTestCase {
    
    func testWithRandomString() {
        self.measureBlock() {
            let randomString = "hello ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i)) ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i)) ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i)) ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i)) ((smily)(appsss)(s)(store)(hihhi hii i(hi)hi hih i)) @his https://www.atlassian.com/software/jira asdd"
            StringParser().parseInputString(randomString) { outputString in
            }
        }
    }
    
    func testWithDefaultString() {
        self.measureBlock() {
            StringParser().parseInputString(defaultValue) { outputString in
            }
        }
    }
    
    func testWithOutUrlString() {
        self.measureBlock() {
            StringParser().parseInputString("@irshad (done)") { outputString in
            }
        }
    }
}
